document.getElementById('add').addEventListener('click', addNumbers);
document.getElementById('sub').addEventListener('click', subNumbers);
document.getElementById('multi').addEventListener('click', multiNumbers);
document.getElementById('div').addEventListener('click', divNumbers);
document.getElementById('mod').addEventListener('click', modNumbers);
document.getElementById('clear').addEventListener('click', clearNumbers);

function addNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 + num2;
	document.getElementById('op').innerHTML = 'Addition';
	return false;

}

function subNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 - num2;
	document.getElementById('op').innerHTML = 'Subtraction';
	return false;
}

function multiNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 * num2;
	document.getElementById('op').innerHTML = 'Multiplication';
	return false;
}

function divNumbers() {
	num1 = document.getElementById('num1').value;
	num2 = document.getElementById('num2').value;

	document.getElementById('result').innerHTML = num1 / num2;
	document.getElementById('op').innerHTML = 'Division';
	return false;
}

function modNumbers() {
	num1 = document.getElementById('num1').value;
	num2 = document.getElementById('num2').value;

	document.getElementById('result').innerHTML = num1 % num2;
	document.getElementById('op').innerHTML = 'Modulus';
	return false;
}

function clearNumbers() {
	document.getElementById('num1').value = '';
	document.getElementById('num2').value = '';
	document.getElementById('result').innerHTML = '';
	document.getElementById('op').innerHTML = '';
}

